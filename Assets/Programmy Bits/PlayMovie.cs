﻿using UnityEngine;
using System.Collections;

public class PlayMovie : MonoBehaviour {

	void OnEnable () {
		Init();
	}
	
	void Update () {
	
	}

	public void Init(){
		((MovieTexture)GetComponent<Renderer>().material.mainTexture).Play();
		((MovieTexture)GetComponent<Renderer>().material.mainTexture).loop = true;
		
	}
}
