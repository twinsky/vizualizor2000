﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MidiDevices : MonoBehaviour {
	
	// midi channel to listen on
	public MidiJack.MidiChannel MidiInputChannel = MidiJack.MidiChannel.All;

	// midi note numbers of BeatStep Pro pads
	const int Pad1 = 44;
	const int Pad2 = 45;
	const int Pad3 = 46;
	const int Pad4 = 47;
	const int Pad5 = 48;
	const int Pad6 = 49;
	const int Pad7 = 50;
	const int Pad8 = 51;
	const int Pad9 = 36;
	const int Pad10 = 37;
	const int Pad11 = 38;
	const int Pad12 = 39;
	const int Pad13 = 40;
	const int Pad14 = 41;
	const int Pad15 = 42;
	const int Pad16 = 43;

	// true if pad is pressed this update
	public bool Pad1Pressed = false;
	public bool Pad2Pressed = false;
	public bool Pad3Pressed = false;
	public bool Pad4Pressed = false;
	public bool Pad5Pressed = false;
	public bool Pad6Pressed = false;
	public bool Pad7Pressed = false;
	public bool Pad8Pressed = false;
	public bool Pad9Pressed = false;
	public bool Pad10Pressed = false;
	public bool Pad11Pressed = false;
	public bool Pad12Pressed = false;
	public bool Pad13Pressed = false;
	public bool Pad14Pressed = false;
	public bool Pad15Pressed = false;
	public bool Pad16Pressed = false;

	// cc controller numbers of BSP knobs
	const int Knob1cc = 16;
	const int Knob2cc = 17;
	const int Knob3cc = 18;
	const int Knob4cc = 19;
	const int Knob5cc = 20;
	const int Knob6cc = 21;
	const int Knob7cc = 22;
	const int Knob8cc = 23;
	const int Knob9cc = 24;
	const int Knob10cc = 25;
	const int Knob11cc = 26;
	const int Knob12cc = 27;
	const int Knob13cc = 28;
	const int Knob14cc = 29;
	const int Knob15cc = 30;
	const int Knob16cc = 31;

	// cc values of BSP knobs
	public float Knob1Value = 0;
	public float Knob2Value = 0;
	public float Knob3Value = 0;
	public float Knob4Value = 0;
	public float Knob5Value = 0;
	public float Knob6Value = 0;
	public float Knob7Value = 0;
	public float Knob8Value = 0;
	public float Knob9Value = 0;
	public float Knob10Value = 0;
	public float Knob11Value = 0;
	public float Knob12Value = 0;
	public float Knob13Value = 0;
	public float Knob14Value = 0;
	public float Knob15Value = 0;
	public float Knob16Value = 0;

	void Start () {
		
	}

	void Update () {
		// check if pads were pressed 
		if (MidiJack.MidiMaster.GetKeyDown (MidiInputChannel, Pad1)) {
			Pad1Pressed = true;
		} else {
			Pad1Pressed = false;
		} 
		if (MidiJack.MidiMaster.GetKeyDown (MidiInputChannel, Pad2)) {
			Pad2Pressed = true;
		} else {
			Pad2Pressed = false;
		} 
		if (MidiJack.MidiMaster.GetKeyDown (MidiInputChannel, Pad3)) {
			Pad3Pressed = true;
		} else {
			Pad3Pressed = false;
		} 
		if (MidiJack.MidiMaster.GetKeyDown (MidiInputChannel, Pad4)) {
			Pad4Pressed = true;
		} else {
			Pad4Pressed = false;
		} 
		if (MidiJack.MidiMaster.GetKeyDown (MidiInputChannel, Pad5)) {
			Pad5Pressed = true;
		} else {
			Pad5Pressed = false;
		} 
		if (MidiJack.MidiMaster.GetKeyDown (MidiInputChannel, Pad6)) {
			Pad6Pressed = true;
		} else {
			Pad6Pressed = false;
		} 
		if (MidiJack.MidiMaster.GetKeyDown (MidiInputChannel, Pad7)) {
			Pad7Pressed = true;
		} else {
			Pad7Pressed = false;
		} 
		if (MidiJack.MidiMaster.GetKeyDown (MidiInputChannel, Pad8)) {
			Pad8Pressed = true;
		} else {
			Pad8Pressed = false;
		} 
		if (MidiJack.MidiMaster.GetKeyDown (MidiInputChannel, Pad9)) {
			Pad9Pressed = true;
		} else {
			Pad9Pressed = false;
		} 
		if (MidiJack.MidiMaster.GetKeyDown (MidiInputChannel, Pad10)) {
			Pad10Pressed = true;
		} else {
			Pad10Pressed = false;
		} 
		if (MidiJack.MidiMaster.GetKeyDown (MidiInputChannel, Pad11)) {
			Pad11Pressed = true;
		} else {
			Pad11Pressed = false;
		} 
		if (MidiJack.MidiMaster.GetKeyDown (MidiInputChannel, Pad12)) {
			Pad12Pressed = true;
		} else {
			Pad12Pressed = false;
		} 
		if (MidiJack.MidiMaster.GetKeyDown (MidiInputChannel, Pad13)) {
			Pad13Pressed = true;
		} else {
			Pad13Pressed = false;
		} 
		if (MidiJack.MidiMaster.GetKeyDown (MidiInputChannel, Pad14)) {
			Pad14Pressed = true;
		} else {
			Pad14Pressed = false;
		} 
		if (MidiJack.MidiMaster.GetKeyDown (MidiInputChannel, Pad15)) {
			Pad15Pressed = true;
		} else {
			Pad15Pressed = false;
		} 
		if (MidiJack.MidiMaster.GetKeyDown (MidiInputChannel, Pad16)) {
			Pad16Pressed = true;
		} else {
			Pad16Pressed = false;
		}
			
		// read knob CC values
		Knob1Value = MidiJack.MidiMaster.GetKnob (MidiInputChannel, Knob1cc); 
		Knob2Value = MidiJack.MidiMaster.GetKnob (MidiInputChannel, Knob2cc); 
		Knob3Value = MidiJack.MidiMaster.GetKnob (MidiInputChannel, Knob3cc); 
		Knob4Value = MidiJack.MidiMaster.GetKnob (MidiInputChannel, Knob4cc); 
		Knob5Value = MidiJack.MidiMaster.GetKnob (MidiInputChannel, Knob5cc); 
		Knob6Value = MidiJack.MidiMaster.GetKnob (MidiInputChannel, Knob6cc); 
		Knob7Value = MidiJack.MidiMaster.GetKnob (MidiInputChannel, Knob7cc); 
		Knob8Value = MidiJack.MidiMaster.GetKnob (MidiInputChannel, Knob8cc); 
		Knob9Value = MidiJack.MidiMaster.GetKnob (MidiInputChannel, Knob9cc); 
		Knob10Value = MidiJack.MidiMaster.GetKnob (MidiInputChannel, Knob10cc); 
		Knob11Value = MidiJack.MidiMaster.GetKnob (MidiInputChannel, Knob11cc); 
		Knob12Value = MidiJack.MidiMaster.GetKnob (MidiInputChannel, Knob12cc); 
		Knob13Value = MidiJack.MidiMaster.GetKnob (MidiInputChannel, Knob13cc); 
		Knob14Value = MidiJack.MidiMaster.GetKnob (MidiInputChannel, Knob14cc); 
		Knob15Value = MidiJack.MidiMaster.GetKnob (MidiInputChannel, Knob15cc); 
		Knob16Value = MidiJack.MidiMaster.GetKnob (MidiInputChannel, Knob16cc); 
	}

}
