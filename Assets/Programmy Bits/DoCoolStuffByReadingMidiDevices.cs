﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityStandardAssets.ImageEffects;

public class DoCoolStuffByReadingMidiDevices : MonoBehaviour {
	public MidiDevices MidiMe;
	public GameObject MovieBackObj;

	public float RotationScalar;
	public float PixelateScalar;
	public float BloomScalar;
	public float AberrationScalar;
	public float TwirlScalar;

	int MovieBackIndex;
	MovieTexture[] MoviesBackground;

	enum MovieTypes{
		BACK,
		MID,
		FRONT
	};

	void Start () {
		LoadNewBank(0, MovieTypes.BACK);
	}
	
	void Update () {
		var mov = MovieBackObj;
		var tweenTime = 1f;

		//knob 1 controls rotation
		var rotTarget = (MidiMe.Knob1Value - 0.5f) * RotationScalar;
		iTween.RotateUpdate(mov, new Vector3(0,0,rotTarget), tweenTime);

		//knob 2 controls pixelation
		int pixy = Mathf.RoundToInt(MidiMe.Knob2Value * PixelateScalar);
		GetComponent<SimplePixelizer>().pixelize = pixy + 1;

		//knob 3 controls bloom
		var bloom = (MidiMe.Knob3Value - 0.5f) * BloomScalar;
		GetComponent<Bloom>().bloomIntensity = bloom;

		//knob 4 controls aberration
		var abby = (MidiMe.Knob4Value - 0.5f) * AberrationScalar;
		GetComponent<VignetteAndChromaticAberration>().chromaticAberration = abby;

		//knob 5 controls twirl
		var twirl = MidiMe.Knob5Value * TwirlScalar;
		GetComponent<Twirl>().angle = twirl;

		//pad 1 cycles movie forward
		if(MidiMe.Pad1Pressed)
			LoadNewMovie(1, MovieTypes.BACK);

		//pad 9 cycles movie back
		if(MidiMe.Pad9Pressed)
			LoadNewMovie(-1, MovieTypes.BACK);
	}

	void LoadNewBank(int bankID, MovieTypes movieType){
		if(movieType == MovieTypes.BACK){
			var movs = Resources.LoadAll("Moovys/Back");
			MoviesBackground = new MovieTexture[movs.Length];
			for(int i=0; i<movs.Length; i++){
				MoviesBackground[i] = (MovieTexture)movs[i];
				MovieBackIndex = 0;
				LoadNewMovie(0, movieType);
			}
		}
	}

	void LoadNewMovie(int indexChange, MovieTypes movieType){
		if(movieType == MovieTypes.BACK){
			MovieBackIndex += indexChange;
			if(MovieBackIndex >= MoviesBackground.Length) MovieBackIndex = 0;
			if(MovieBackIndex < 0) MovieBackIndex = MoviesBackground.Length - 1;
			MovieBackObj.GetComponent<Renderer>().material.mainTexture = MoviesBackground[MovieBackIndex];
			MovieBackObj.GetComponent<PlayMovie>().Init();
		}
	}
}
