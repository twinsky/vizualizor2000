﻿using UnityEngine;
using System.Collections;
using Rewired;
using DG.Tweening;
using UnityStandardAssets.ImageEffects;

public class ControlStuff : MonoBehaviour {
	public GameObject[] MovieObjs;
	public int PlayerID;
	public float TiltAmount;
	public float ZoomInAmount;
	public float ZoomOutAmount;
	public float ZoomTime;
	public float BloomAmount;
	public float BloomTime;
	public float AberrationAmount;
	public float AberrationTime;
	public int PixelateAmount;
	public float PixelateTime;
	public float MovieSwitchTime;

	int DefaultZoomLevel = 60;
	int ActiveMovieIndex;
	Player Ctrl;
	Camera cam;


	void Start () {
		Ctrl = ReInput.players.GetPlayer(PlayerID);
		cam = GetComponent<Camera>();
	}
	
	void Update () {
		if(Ctrl.GetButtonDown("ZoomIn")) ZoomIn();
		if(Ctrl.GetButtonDown("ZoomOut")) ZoomOut();
		if(Ctrl.GetButtonDown("Bloom")) Bloom();
		if(Ctrl.GetButtonDown("Aberration")) Aberration();
		if(Ctrl.GetButtonDown("Pixelate")) Pixelate();
		if(Ctrl.GetButtonDown("ChangeMovie")) ChangeMovie(1);

		Tilt();
	}

	void ZoomIn(){
		DOTween.To(()=> cam.fieldOfView, x => cam.fieldOfView = x, ZoomInAmount, ZoomTime).OnComplete(ZoomRestore);
	}

	void ZoomOut(){
		var targetZoom = cam.fieldOfView + ZoomOutAmount;
		DOTween.To(()=> cam.fieldOfView, x => cam.fieldOfView = x, targetZoom, ZoomTime);
	}

	void ZoomRestore(){
		DOTween.To(()=> cam.fieldOfView, x => cam.fieldOfView = x, DefaultZoomLevel, ZoomTime);
	}

	void Bloom(){
		var bloom = GetComponent<Bloom>();
		bloom.bloomIntensity = 0;
		DOTween.Kill("bloom");
		DOTween.To(()=> bloom.bloomIntensity, x => bloom.bloomIntensity = x, BloomAmount, BloomTime).SetLoops(2).SetId("bloom");
	}

	void Aberration(){
		var abs = GetComponent<VignetteAndChromaticAberration>();
		abs.chromaticAberration = 0;
		DOTween.Kill("aberration");
		DOTween.To(()=> abs.chromaticAberration, x => abs.chromaticAberration = x, AberrationAmount, AberrationTime).SetLoops(2).SetId("aberration");
	}

	void Pixelate(){
		var pix = GetComponent<SimplePixelizer>();
		pix.pixelize = 1;
		DOTween.Kill("pixelate");
		DOTween.To(()=> pix.pixelize, x => pix.pixelize = x, PixelateAmount, PixelateTime).SetLoops(2).SetId("pixelate");
	}

	void Tilt(){
		var tilty = Ctrl.GetAxis("Tilt");
		tilty *= TiltAmount;

		transform.eulerAngles = new Vector3(0, 0, tilty);
	}

	void ChangeMovie(int shiftAmount){
		var oldMovie = MovieObjs[ActiveMovieIndex];
		ActiveMovieIndex += shiftAmount;
		if(ActiveMovieIndex < 0) ActiveMovieIndex += MovieObjs.Length;
		else if(ActiveMovieIndex >= MovieObjs.Length) ActiveMovieIndex -= MovieObjs.Length;

		var oldMovMat = oldMovie.GetComponent<Renderer>().material;
		oldMovMat.DOFade(0, MovieSwitchTime);

		var newMov = MovieObjs[ActiveMovieIndex];
		newMov.SetActive(true);
		var newMovMat = newMov.GetComponent<Renderer>().material;
		newMovMat.color = new Color(1,1,1,0);
		newMovMat.DOFade(1, MovieSwitchTime);

		newMov.transform.localPosition = new Vector3(0,0,-5);
		newMov.transform.DOLocalMove(Vector3.zero, MovieSwitchTime);
	}
}
